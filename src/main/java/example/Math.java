package example;

public class Math {
    static int fib(int n) {
        int prev = 0;
        int curr = java.lang.Math.min(1, n);

        for(int i = 1; i < n; i++) {
            int temp = curr;
            curr += prev;
            prev = temp;
        }

        return curr;
    }
}
