package example

import org.scalacheck.Gen

object Arbitrary {
  val choose2to127: org.scalacheck.Arbitrary[Int] = org.scalacheck.Arbitrary(Gen.chooseNum[Int](2, 127))
}
