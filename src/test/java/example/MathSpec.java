package example;

import org.scalacheck.*;
import org.scalacheck.util.Pretty;
import scala.math.Numeric;

public class MathSpec extends Properties {
    private static final Numeric.IntIsIntegral$ INTEGER = Numeric.IntIsIntegral$.MODULE$;

    public MathSpec() {
        super("Fib-Java");

        Shrink shrinkInt = Shrink.shrinkIntegral(INTEGER);

        property().update("definition", () -> Prop.forAll(
                (Integer n) -> Math.fib(n) == Math.fib(n - 1) + Math.fib(n - 2),
                Prop::propBoolean,
                (org.scalacheck.Arbitrary)(Arbitrary$.MODULE$.choose2to127()),
                shrinkInt,
                Pretty::prettyAny
        ));
    }
}
