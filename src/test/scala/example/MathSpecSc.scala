package example

import org.scalacheck.{Gen, Properties}
import org.scalacheck.Prop.forAll

object MathSpecSc extends Properties("Fib-Scala") {
  property("definition") = forAll(Gen.chooseNum[Int](2, 127)) { n: Int =>
    Math.fib(n) == Math.fib(n - 1) + Math.fib(n - 2)
  }
}